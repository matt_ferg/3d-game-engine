
OBJS = main.cc Display.cc Mesh.cc Shader.cc Texture.cc Transform.cc stb_image.c 

CC = g++

COMPILER_FLAGS = -Wall -Wextra -Werror

COMPILER_FREES = -Wno-missing-field-initializers \
	-Wno-unused-but-set-variable \
	-Wno-misleading-indentation \
	-Wno-shift-negative-value \
	-Wno-unused-parameter

LINKER_FLAGS = -lSDL2 -lGL -lGLEW
OBJ_NAME = hello_world

all : $(OBJS)
	$(CC) $(COMPILER_FLAGS) $(COMPILER_FREES) $(OBJS) $(LINKER_FLAGS) -o $(OBJ_NAME)

clean : 
	rm *.o

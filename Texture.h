#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <GL/glew.h>

class Texture
{
public:
   Texture(const std::string& filename);
   void bind(unsigned int unit);
   virtual ~Texture();
protected:
private:
   //Texture(const Texture& other) {}
   //Texture& operator=(const Texture& other) {}
   //handle to texture 
   GLuint m_texture;
};
#endif //Texture.h

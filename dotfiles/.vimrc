set nocompatible
syntax enable
filetype on
set nu
set showcmd
set cursorline
set wildmenu
set showmatch
set incsearch
set hlsearch
set laststatus=2
nnoremap <leader><space> :nohlsearch<cr>
set foldenable
set foldlevelstart=1
nnoremap j gj
nnoremap k gk
augroup configgroup
	autocmd!
	autocmd VimEnter * highlight clear SignColumn
	autocmd FileType cpp setlocal list
	autocmd FileType cpp setlocal tabstop=3
	autocmd FileType cpp setlocal softtabstop=3
	autocmd FileType cpp setlocal shiftwidth=3
	autocmd FileType cpp setlocal expandtab
augroup END

"make error to open
nnoremap <f9> :make<bar>copen<cr>

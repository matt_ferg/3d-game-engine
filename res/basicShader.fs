#version 120

//varying variable shared between shaders
varying vec2 tex_coord0;

//variable both cpu and gpu have access to 
uniform sampler2D diffuse;

void main()
{
	//send the coordinates to read
   gl_FragColor = texture2D(diffuse, tex_coord0); //vec4(1.0, 0, 0, 1.0);
}

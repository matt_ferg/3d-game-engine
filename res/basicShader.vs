#version 120

//attribute of mesh
attribute vec3 position;

//texCoord comes from mesh
attribute vec2 texCoord;

varying vec2 tex_coord0;

// holds the matrix transform of the mesh
uniform mat4 transform;

void main()
{
	//the second item controls how much the matrix controls the movement amount
   gl_Position = transform * vec4(position, 1.0);
   tex_coord0 = texCoord;
}

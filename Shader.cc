/*
###############################################################################
##
##  name: Shader.cc
##
##  purpose: contains shader logic
##
##  21OCT18  M. Ferguson
##           Initial development.
##
###############################################################################
*/

#include "Shader.h"
#include <iostream>
#include <fstream>

Shader::Shader(const std::string& filename)
{
   //some number opengl recognizes our program by
   m_program = glCreateProgram();
   m_shaders[0] = createShader(loadShader(filename + ".vs"), GL_VERTEX_SHADER);
   m_shaders[1] = createShader(loadShader(filename + ".fs"), GL_FRAGMENT_SHADER);
   //add our shaders to our program
   for(size_t i = 0; i< NUM_SHADERS; ++i)
   {
      glAttachShader(m_program, m_shaders[i]);
   }
   // tell opengl what part of the data to read in to what variables
   glBindAttribLocation(m_program, 0, "pos");
   glBindAttribLocation(m_program, 1, "texCoord");
   //link our shaders to our program
   glLinkProgram(m_program);
   checkShaderError(m_program, GL_LINK_STATUS, true, "Error: Program linking failed: ");
   //validating our shaders to our program
   glValidateProgram(m_program);
   checkShaderError(m_program, GL_VALIDATE_STATUS, true, "Error: Program is invalid: ");

   //now we have access to the transform uniform in the shader
   m_uniforms[TRANSFORM_U] = glGetUniformLocation(m_program, "transform");
}

Shader::~Shader()
{
   for(size_t i = 0; i< NUM_SHADERS; ++i)
   {
      glDetachShader(m_program, m_shaders[i]);
      glDeleteShader(m_shaders[i]);
   }
   glDeleteProgram(m_program);
}

std::string Shader::loadShader(const std::string& filename)
{
   std::ifstream file;
   file.open((filename).c_str());
   //
   std::string output;
   std::string line;
   //
   if(file.is_open())
   {
      while(file.good())
      {
         getline(file, line);
         output.append(line + "\n");
      }
   }
   else
   {
     std::cerr << "Unable to load shader: " << filename << std::endl;
   }
   return output;
}

void Shader::checkShaderError(GLuint shader,
                              GLuint flag,
                              bool isProgram,
                              const std::string& errorMessage)
{
   GLint success = 0;
   GLchar error[1024] = { 0 };
   //
   if(isProgram)
      glGetProgramiv(shader, flag, &success);
   else
      glGetShaderiv(shader, flag, &success);
   if(success == GL_FALSE)
   {
      if(isProgram)
         glGetProgramInfoLog(shader, sizeof(error), NULL, error);
      else
         glGetShaderInfoLog(shader, sizeof(error), NULL, error);

      std::cerr << errorMessage << ": '" << error << "'" << std::endl;
   }
}

GLuint Shader::createShader(const std::string& text, GLenum shader_type)
{
   GLuint shader(glCreateShader(shader_type));
   //
   if(shader == 0)
   {
      std::cerr << "Error compiling shader type " << shader_type << std::endl;
   }
   //
   const GLchar* shader_source_strings[1];
   shader_source_strings[0] = text.c_str();
   GLint source_strings_lengths[1];
   source_strings_lengths[0] = text.length();
   //
   glShaderSource(shader, 1, shader_source_strings, source_strings_lengths);
   glCompileShader(shader);
   //
   checkShaderError(shader, GL_COMPILE_STATUS, false, "Error compiling shader!");
   //
   return shader;
}

void Shader::bind()
{
   glUseProgram(m_program);
}

void Shader::update(const Transform& transform)
{
   //get the model matrix
   glm::mat4 model = transform.GetModel();

   //update the uniform 4x4 matrix of the transform at handle TRANSFORM_U
   //watchout for transposed matrices
   glUniformMatrix4fv(m_uniforms[TRANSFORM_U], 1, GL_FALSE, &model[0][0]);
}















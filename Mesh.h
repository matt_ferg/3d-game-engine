/*
###############################################################################
##
##  name: Mesh.h
##
##  purpose: contains mesh data and logic
##
##  21OCT18  M. Ferguson
##           Initial development.
##
###############################################################################
*/

#ifndef MESH_H
#define MESH_H

#include <GL/glew.h>
#include <glm/glm.hpp>

/*#############################################################################
#
#  CLASS Vertex
#
###############################################################################
*/
class Vertex 
{
public:
   Vertex(const glm::vec3& input, const glm::vec2& tex_coord)
   {
      m_Pos = input;
      m_TexCoord = tex_coord; 
   };
   
   inline const glm::vec3* getPos() const { return &m_Pos; }
   inline const glm::vec2* getTexCoord() const { return &m_TexCoord; }
protected:
private:
   glm::vec3 m_Pos;
   //reading pixels depending where we are on the mesh
   //specify some location on the texture for each vertex
   // interpolate between these two points and draw that pixel
   //position on the texture we're mapping to the vertex
   glm::vec2 m_TexCoord;
};

/*#############################################################################
#
#  CLASS Mesh
#
###############################################################################
*/
class Mesh
{
public:
   Mesh(Vertex* vertices, unsigned int numVertices);
   void draw();
   virtual ~Mesh();
protected:
private:
   //Mesh(const Mesh& other);
   //void operator=(const Mesh& other);

   enum
   {
      //vertex buffers
      POSITION_VB,
      TEXCOORD_VB,
      NUM_BUFFERS
      
   };

   GLuint m_vertexArrayObject;
   GLuint m_vertexArrayBuffers[NUM_BUFFERS];
   unsigned int m_drawCount;
};
#endif //MESH_H














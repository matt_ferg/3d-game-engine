#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

class Transform
{
public:
   Transform();
   Transform(const glm::vec3& pos, const glm::vec3& rot, const glm::vec3& scale);

   //contains position rotation and scale
   glm::mat4 GetModel() const;

   inline glm::vec3& getPos() { return m_position; }
   inline glm::vec3& getRot() { return m_rotation; }
   inline glm::vec3& getScl() { return m_scale; }

   inline void setPos(glm::vec3& pos) { m_position = pos; }
   inline void setRot(glm::vec3& rot) { m_rotation = rot; }
   inline void setScl(glm::vec3& scl) { m_scale = scl; }

protected:
private:
   glm::vec3 m_position;
   glm::vec3 m_rotation;
   glm::vec3 m_scale;
   //~Transform();
   //Transform& operator=(const Transform& other);
};

#endif //TRANFORM_H

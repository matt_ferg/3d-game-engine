/*
###############################################################################
##
##  name: Shader.h
##
##  purpose: contains Shader data and logic
##
##  21OCT18  M. Ferguson
##           Initial development.
##
###############################################################################
*/

#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <GL/glew.h>
#include "Transform.h"

class Shader
{
public:
   Shader(const std::string& fileName);

   //sets the gpu to using these shaders (not any others)
   void bind();
   static std::string loadShader(const std::string&);
   static void checkShaderError(GLuint, GLuint, bool, const std::string&);
   static GLuint createShader(const std::string&, unsigned int);
   //updates the uniforms in our array to a new set of values {transforms, ..
   void update(const Transform& transform);
   virtual ~Shader();
protected:
private:
   //Shader(const Shader& other) {}
   //void operator=(const Shader& other) {}
   //can replace this static int with enum for diff types of shaders
   //enum to represent our uniforms in the shaders {vertex, 
   enum
   {
      TRANSFORM_U,
      
      NUM_UNIFORMS
   };
   static const unsigned int NUM_SHADERS = 2;
   GLuint m_program;
   GLuint m_shaders[NUM_SHADERS];
   GLuint m_uniforms[NUM_UNIFORMS];
};


#endif

#include "Transform.h"

Transform::Transform() :
   m_position(0.0f),
   m_rotation(0.0f),
   m_scale(1.0f)
   {}


Transform::Transform(const glm::vec3& pos,
                     const glm::vec3& rot, 
                     const glm::vec3& scl = glm::vec3(1.0f, 1.0f, 1.0f)) :
   m_position(pos),
   m_rotation(rot),
   m_scale(scl) 
   {}

glm::mat4 Transform::GetModel() const
{
   glm::mat4 posMatrix = glm::translate(m_position);
   //careful for rotations using matricies 
   //can get gymbal lock
   //should use quaternions
   glm::mat4 rotXMatrix = glm::rotate(m_rotation.x, glm::vec3(1,0,0));
   glm::mat4 rotYMatrix = glm::rotate(m_rotation.y, glm::vec3(0,1,0));
   glm::mat4 rotZMatrix = glm::rotate(m_rotation.z, glm::vec3(0,0,1));
   glm::mat4 scaleMatrix = glm::scale(m_scale);
   //combine them simply by multiplying them, dangerous because not commutative property of matrices

   //runs in reverse order from right to left
   glm::mat4 rotMatrix = rotZMatrix * rotYMatrix * rotXMatrix;
   //gets to appropriate size, rotates it, puts it in position in the world
   return posMatrix * rotMatrix * scaleMatrix;
}

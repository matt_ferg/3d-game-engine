/*
###############################################################################
##
##  name: Mesh.cc
##
##  purpose: contains mesh logic
##
##  21OCT18  M. Ferguson
##           Initial development.
##
###############################################################################
*/

#include "Mesh.h"
#include <iostream>
#include <vector>

Mesh::Mesh(Vertex* vertices, const unsigned int numVertices)
{
   const char* const MODULE("Mesh::Mesh");

   std::clog << "entering " << MODULE << std::endl;

   m_drawCount = numVertices;

   glGenVertexArrays(1, &m_vertexArrayObject);
   glBindVertexArray(m_vertexArrayObject);

   //must add data to vecs because it's not contiguous in the vertices buffer when used in ... ?
   std::vector<glm::vec3> positions;
   std::vector<glm::vec2> texCoords;

   //holding the positions 
   positions.reserve(numVertices);
   texCoords.reserve(numVertices);

   for(size_t i=0; i<numVertices; ++i)
   {
      //possible optimization to pass as references into reference vector instead of copying
      positions.push_back(*vertices[i].getPos());
      texCoords.push_back(*vertices[i].getTexCoord());
   }

   glGenBuffers(NUM_BUFFERS, m_vertexArrayBuffers);
   {
      glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[POSITION_VB]);
      //this buffer is an array and put all of our vertices into this array
      glBufferData(GL_ARRAY_BUFFER,
                   numVertices * sizeof(positions[0]),
                   &positions[0],
                   GL_STATIC_DRAW);

      //describe the attributes as a sequential array of data to the gpu
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
   }

   {
      glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[TEXCOORD_VB]);
      //this buffer is an array and put all of our vertices into this array
      glBufferData(GL_ARRAY_BUFFER,
                   numVertices * sizeof(texCoords[0]),
                   &texCoords[0],
                   GL_STATIC_DRAW);

      //describe the attributes as a sequential array of data to the gpu
      glEnableVertexAttribArray(1);
      glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
   }

   glBindVertexArray(0);

   std::clog << "exiting " << MODULE << std::endl;
}

void Mesh::draw()
{
   //const char* const MODULE("Mesh::draw");

   //std::clog << "entering " << MODULE << std::endl;

   glBindVertexArray(m_vertexArrayObject);
   glDrawArrays(GL_TRIANGLES, 0, m_drawCount);

   //std::clog << "exiting " << MODULE << std::endl;
}

Mesh::~Mesh()
{
   glDeleteVertexArrays(1, &m_vertexArrayObject);
}

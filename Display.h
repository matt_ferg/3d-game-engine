#ifndef DISPLAY_H
#define DISPLAY_H

#include <string>
#include <SDL2/SDL.h>

class Display
{
public:
   Display(const unsigned int width, const unsigned int height, const std::string& title);
   void update();
   void clear(float, float, float, float);
   bool isClosed();
   virtual ~Display();
protected:
private:
   SDL_Window* m_window;
   SDL_GLContext m_glContext;
   bool m_isClosed;
// Display(const Display& other) {}
// Display& operator=(const Display& other) {}
};
#endif //Display.h

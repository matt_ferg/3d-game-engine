/*
###############################################################################
##
##  name: Display.cc
##
##  purpose: contains display window logic
##
##  21OCT18  M. Ferguson
##           Initial development.
##
###############################################################################
*/

#include <iostream>
#include <GL/glew.h>
#include "Display.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"
#include "Transform.h"

int main(int argc, char** argv)
{

   //get an arg parser going for some command line args {verbose, safemode}
   //get a static trace class for tracing class methods
   const char* const MODULE("MAIN");

   std::string verboseArg("-v");

   std::clog << "entering " << MODULE << std::endl;

   Display* display(new Display(800 , 600, "hello world"));
   //meshes are mapped as origin at center center
   // top left is -1, -1
   //textures are mapped as origin at bottom left
   // top left is 0, 1
   Vertex vertices1[] = { Vertex(glm::vec3(-0.5,-0.5,0), glm::vec2(0.0, 0.0)),
                         Vertex(glm::vec3(0,0.5,0), glm::vec2(0.5,1.0)),
                         Vertex(glm::vec3(0.5,-0.5,0), glm::vec2(1.0,0.0)) };
   Mesh mesh1(vertices1, sizeof(vertices1)/sizeof(vertices1[0]));
/*
   Vertex vertices2[] = { Vertex(glm::vec3(-0.5,0.5,0), glm::vec2(0.0,1.0)),
                         Vertex(glm::vec3(0.5,0.5,0), glm::vec2(1.0,1.0)),
                         Vertex(glm::vec3(0,-0.5,0), glm::vec2(0.5,0.0)) };
   Mesh mesh2(vertices2, sizeof(vertices2)/sizeof(vertices2[0]));
*/

   Shader shader("./res/basicShader");
   //see more into texture mapping
   // interpolating which pixels to draw on the mesh
   Texture texture("./res/bricks.jpg");
   Transform transform;

   while(!display->isClosed())
   {
      display->clear(0.0f, 0.15f, 0.3f, 1.0f);
      shader.bind();
      texture.bind(0);
      shader.update(transform);
      mesh1.draw();

      display->update();

   /*
      display->clear(0.0f, 0.15f, 0.3f, 1.0f);
      shader.Bind();
      texture.Bind(0);
      mesh2.draw();
      display->update();
   */
   }

   std::clog << "exiting " << MODULE << std::endl;
   return 0;
};




















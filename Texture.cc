#include "Texture.h"
#include "stb_image.h"
#include <cassert>
#include <iostream>

//see how your texture loader loads data vs. stb_image does

Texture::Texture(const std::string& fileName)
{
   //figure out how your texture loader loads textures, this is specific to that system
   const char* const MODULE("Texture::Texture");

   std::clog << "entering " << MODULE << std::endl;

   int width, height, num_components;
   //loads the texture
   unsigned char* image_data(stbi_load(fileName.c_str(), &width, &height, &num_components, 4));

   if(image_data == NULL)
   {
      std::cerr << "Texture loading failed for texture: " << fileName << std::endl;
   }
   //have space for a texture
   glGenTextures(1, &m_texture);
   //bind texture 3d, volume, 2d
   glBindTexture(GL_TEXTURE_2D, m_texture);

   //texture params control texture wrapping width, height if outside texture
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
   
   //how to handle if texture takes up more or less pixels than the defined resolution
   glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
   glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);

   stbi_image_free(image_data);

   std::clog << "exiting " << MODULE << std::endl;
}

Texture::~Texture()
{
   //remove each texture 
   glDeleteTextures(1, &m_texture);
}

void Texture::bind(unsigned int unit)
{
   //const char* const MODULE("Texture::Bind");
   //std::clog << "entering " << MODULE << std::endl;

   assert(unit <= 31);

   //select which texture
   //active textures are stored sequentially
   //
   //glActiveTexture(GL_TEXTURE0);
   glBindTexture(GL_TEXTURE_2D, m_texture);

   //std::clog << "exiting " << MODULE << std::endl;
}





















